<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="user", indexes={
 * @ORM\Index(name="user_uuid", columns={"UUID"})
 * })
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="uuid", unique=true)
     */
    private $UUID;

    /**
     * @ORM\OneToMany(
     *      targetEntity="Review",
     *      mappedBy="user",
     *      orphanRemoval=true
     * )
     */
    private $reviews;

    public function __construct()
    {
        $this->reviews = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uUID
     *
     * @param string $uUID
     *
     * @return User
     */
    public function setUUID($uUID)
    {
        $this->UUID = $uUID;

        return $this;
    }

    /**
     * Get uUID
     *
     * @return string
     */
    public function getUUID()
    {
        return $this->UUID;
    }

    /**
     * Add review
     *
     * @param \AppBundle\Entity\Review $review
     *
     * @return User
     */
    public function addReview(\AppBundle\Entity\Review $review)
    {
        $this->reviews[] = $review;

        return $this;
    }

    /**
     * Remove review
     *
     * @param \AppBundle\Entity\Review $review
     */
    public function removeReview(\AppBundle\Entity\Review $review)
    {
        $this->reviews->removeElement($review);
    }

    /**
     * Get reviews
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReviews()
    {
        return $this->reviews;
    }
}
