<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Review;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;

class LoadUserData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUUID("UUID2");
        $manager->persist($user);

        $user = new User();
        $user->setUUID("UUID1");
        $manager->persist($user);

        $review = new Review();
        $review->setRating(10);
        $review->setUser($user);
        $manager->persist($review);

        $review = new Review();
        $review->setRating(90);
        $review->setUser($user);
        $manager->persist($review);

        $manager->flush();
    }
}