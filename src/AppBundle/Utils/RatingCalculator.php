<?php

namespace AppBundle\Utils;

use Doctrine\ORM\EntityRepository;


class RatingCalculator implements RatingCalculatorInterface
{
    /**
     * @var EntityRepository
     */
    private $reviewRepository;

    /**
     * @var EntityRepository
     */
    private $userRepository;

    public function __construct(EntityRepository $reviewRepository, EntityRepository $userRepository)
    {
        $this->reviewRepository = $reviewRepository;
        $this->userRepository = $userRepository;
    }

    public function getRating($userUUID)
    {
        $row = $this->userRepository
        ->createQueryBuilder('u')
        ->select("u.id")
        ->where("u.UUID = :UUID")
        ->setParameter('UUID', $userUUID)
        ->getQuery()
        ->getOneOrNullResult();

        if (!$row) {
            throw new RatingException("There is no user with this UUID");
        }
        $id = $row['id'];

        return $this->getRatingByUserId($id);

    }

    /**
     * @param $userId
     * @return float
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function getRatingByUserId($userId)
    {
        $result = $this->reviewRepository
            ->createQueryBuilder('r')
            ->select("SUM(r.rating) / COUNT(r.id)")
            ->where("r.user = :user")
            ->setParameter('user', $userId)
            ->getQuery()
            ->getSingleResult();

        return round($result[1]);
    }
}
