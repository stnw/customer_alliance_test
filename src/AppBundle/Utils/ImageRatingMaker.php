<?php

namespace AppBundle\Utils;



class ImageRatingMaker
{
    /**
     * @var RatingCalculatorInterface
     */
    private $ratingCalculator;

    public function __construct(RatingCalculatorInterface $ratingCalculator)
    {
        $this->ratingCalculator = $ratingCalculator;
    }

    /**
     * @param $userUUID
     * @return string
     */
    public function getImage($userUUID)
    {
        $rating = $this->ratingCalculator->getRating($userUUID);

        $image = imagecreatetruecolor(100, 100);
        $bg = imagecolorallocate($image, 0, 66, 255);
        imagefill($image, 0, 0, $bg);
        $color = imagecolorallocate($image, 255, 255, 255);
        imagestring($image, 10, 35, 35,   $rating . '%', $color);
        ob_start();
        imagepng($image);
        $imageString = ob_get_contents();
        ob_end_clean();
        imagedestroy($image);

        return $imageString;
    }
}