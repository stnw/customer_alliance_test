<?php

namespace AppBundle\Utils;

interface RatingCalculatorInterface
{
    public function getRating($userUUID);
}
