<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Utils\RatingException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;

class RatingController extends Controller
{

    /**
     * @Route("rating/{UUID}.png", name="rating_show_png")
     */
    public function showRatingPNG($UUID)
    {
        try {
            $image = $this->get('app.utils.rating_image_creator')->getImage($UUID);
        } catch (RatingException $e) {
            throw new NotFoundHttpException($e->getMessage());
        }

        return new Response($image, 200, ['Content-Type'     => 'image/png']);
    }


    /**
     * @Route("rating/{UUID}", name="rating_show")
     */
    public function showAction($UUID)
    {
        try {
            $rating = $this->get('app.utils.rating_calculator')->getRating($UUID);
        } catch (RatingException $e) {
            throw new NotFoundHttpException($e->getMessage());
        } catch (\Exception $e) {
            $rating = 0;
            //to log here if we have a special logger or remove it and catch the error on email
        }

        return $this->render('rating/show.html.twig', [
                'rating' => $rating
            ]
        );
    }


}