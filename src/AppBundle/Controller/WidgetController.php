<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class WidgetController extends Controller
{
    /**
     * @Route("widget/{UUID}.js", name="widget_show")
     */
    public function showAction($UUID)
    {
        $rendered = $this->renderView('widget/show.js.twig', [
            'url' => $this->generateUrl('rating_show', ['UUID' => $UUID])
        ]);
        $response = new Response($rendered);
        $response->headers->set('Content-Type', 'text/javascript');

        return $response;
    }
}