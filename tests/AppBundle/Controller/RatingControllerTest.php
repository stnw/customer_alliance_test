<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\DataFixtures\ORM\LoadUserData;

use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;

class RatingControllerTest extends WebTestCase
{
    public function setup() {
        $client = static::createClient();
        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();

        $loader = new Loader();
        $loader->addFixture(new LoadUserData);
        $purger = new ORMPurger($entityManager);
        $executor = new ORMExecutor($entityManager, $purger);
        $executor->execute($loader->getFixtures());

        parent::setUp();
    }

    public function testRating()
    {
        $client = static::createClient();
        /** @var ContainerInterface $container */
        $container = $client->getContainer();
        $UUID = 'UUID1';
        $crawler = $client->request('GET', $container->get('router')->generate('rating_show', ['UUID' => $UUID]));

        $this->assertEquals(1, $crawler->filter('div.widget_rating:contains("50%")')->count());
    }
}