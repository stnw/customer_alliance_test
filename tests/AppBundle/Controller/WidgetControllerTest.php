<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\Comment;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\DataFixtures\ORM\LoadUserData;

class WidgetControllerTest extends WebTestCase
{
    public function testRating()
    {
        $client = static::createClient();
        /** @var ContainerInterface $container */
        $container = $client->getContainer();
        $UUID = 'uuid';
        $client->request('GET', $container->get('router')->generate('widget_show', ['UUID' => $UUID]));

        $url = $container->get('router')->generate('rating_show', ['UUID' => $UUID]);
        $code = 'document.writeln(\'<div style="position: fixed; right: 0; bottom: 0; z-index: 100000; width: 100px; height: 100px;"><iframe src="' . $url . '" name="MagicIframe" width="100%" height="100%" scrolling="no" marginheight="0" marginwidth="0" frameborder="0"></iframe></div>\');';
        $data = $client->getResponse()->getContent();
        $this->assertEquals($code, $data);
    }
}
